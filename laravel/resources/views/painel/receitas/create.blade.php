@extends('painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Cadastrar Receita</h2>

	        <hr>

	        @include('painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.receitas.store') }}" method="post" enctype="multipart/form-data">

  			<input type="hidden" name="_token" value="{!! csrf_token() !!}">

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-group">
  						<label for="inputTitulo">Título</label>
  						<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}">
  					</div>

            <div class="form-group">
              @if(old('imagem'))
                Thumb atual<br>
                <img src="assets/img/receitas/{{old('imagem')}}"><br>
              @endif
              <label for="inputThumb">Thumb</label>
              <input type="file" class="form-control" id="inputFoto" required name="imagem">
            </div>

            <div class="form-group">
  						<label for="inputIngredientes">Ingredientes</label>
  						<textarea name="ingredientes" id="inputIngredientes" class="form-control">{{old('ingredientes')}}</textarea>
  					</div>

            <div class="form-group">
  						<label for="inputModoPreparo">Modo de Preparo</label>
  						<textarea name="preparo" id="inputModoPreparo" class="form-control">{{old('preparo')}}</textarea>
  					</div>

            <div class="form-group">
  						<label for="inputTPPreparo">Tempo de Preparo</label>
  						<input type="text" name="tempo_preparo" class="form-control" id="inputTPPreparo" value="{{old('tempo_preparo')}}">
  					</div>

            <div class="form-group">
  						<label for="inputRendimento">Rendimento</label>
  						<input type="text" name="rendimento" class="form-control" id="inputRendimento" value="{{old('rendimento')}}">
  					</div>

            <hr>

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.receitas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
