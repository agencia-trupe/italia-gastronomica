@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>Receitas</h2>

      @include('painel.partials.mensagens')

      <hr>

      <a href="{{ URL::route('painel.receitas.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Receita</a>

      <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="receitas">

        <thead>
          <tr>
            <th>Ordenad</th>
            <th>Título</th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
          </tr>
        </thead>

        <tbody>
          @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
              <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
              <td>
                {{ $registro->titulo }}
              </td>
              <td class="crud-actions">
                <a href="{{ URL::route('painel.receitas.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                <form action="{{ URL::route('painel.receitas.destroy', $registro->id) }}" method="post">
                  <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>

      </table>
    </div>
  </div>
</div>

@endsection
