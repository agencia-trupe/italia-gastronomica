@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>Usuários</h2>

      @include('painel.partials.mensagens')

      <hr>

      <a href="{{ URL::route('painel.usuarios.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Usuário</a>

      <table class="table table-striped table-bordered table-hover ">

        <thead>
          <tr>
            <th>Usuário</th>
            <th>E-mail</th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
          </tr>
        </thead>

        <tbody>
          @foreach ($usuarios as $usuario)

            <tr class="tr-row">
              <td>
                {{ $usuario->login }}
              </td>
              <td>
                {{ $usuario->email }}
              </td>
              <td class="crud-actions">
                <a href="{{ URL::route('painel.usuarios.edit', $usuario->id ) }}" class="btn btn-primary btn-sm">editar</a>

                @if(Auth::user()->id != $usuario->id)
                  <form action="{{ URL::route('painel.usuarios.destroy', $usuario->id) }}" method="post">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
                @endif
              </td>
            </tr>
          @endforeach
        </tbody>

      </table>
    </div>
  </div>
</div>

@endsection
