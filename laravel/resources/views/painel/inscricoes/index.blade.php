@extends('painel.template')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>Inscrições</h2>

      @include('painel.partials.mensagens')

      <hr>

      <a href="{{ url('painel/download-inscricoes') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-download"></span> Extrair Cadastros</a>

      <table class="table table-striped table-bordered table-hover ">

        <thead>
          <tr>
            <th>Nome</th>
            <th>CPF</th>
            <th>E-mail</th>
            <th>Cupons Gerados</th>
            <th><span class="glyphicon glyphicon-cog"></span></th>
          </tr>
        </thead>

        <tbody>
          @foreach ($registros as $registro)

            <tr class="tr-row">
              <td>
                {{ $registro->nome }}
              </td>
              <td>
                {{ $registro->cpf }}
              </td>
              <td>
                {{ $registro->email }}
              </td>
              <td>
                {{ count($registro->cupons) }}
              </td>
              <td class="crud-actions" style="white-space: nowrap;">
                <a href="{{ url('/painel/inscricoes/detalhes/'.$registro->id) }}" class="btn btn-primary btn-sm">visualizar</a>

                <form action="{{ URL::route('painel.inscricoes.destroy', $registro->id) }}" method="post">
                  <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                  <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>

      </table>
    </div>
  </div>
</div>

@endsection
