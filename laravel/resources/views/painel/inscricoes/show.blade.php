@extends('painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

		    <h2>
	      	Visualizar Inscrição
	      </h2>

	      <hr>

        <div class="form-group">
				  <label>Data da Inscrição</label>
					<input type="text" class="form-control" value="{{ $registro->created_at->format('d/m/Y H:i:s') }}" readonly>
				</div>

        <div class="form-group">
				  <label>Nome</label>
					<input type="text" class="form-control" value="{{ $registro->nome }}" readonly>
				</div>

        <div class="form-group">
				  <label>CPF</label>
					<input type="text" class="form-control" value="{{ $registro->cpf }}" readonly>
				</div>

        <div class="form-group">
				  <label>CEP</label>
					<input type="text" class="form-control" value="{{ $registro->cep }}" readonly>
				</div>

        <div class="form-group">
				  <label>Endereço</label>
					<input type="text" class="form-control" value="{{ $registro->endereco }}" readonly>
				</div>

        <div class="form-group">
				  <label>Número</label>
					<input type="text" class="form-control" value="{{ $registro->numero }}" readonly>
				</div>

        <div class="form-group">
				  <label>Complemento</label>
					<input type="text" class="form-control" value="{{ $registro->complemento }}" readonly>
				</div>

        <div class="form-group">
				  <label>Bairro</label>
					<input type="text" class="form-control" value="{{ $registro->bairro }}" readonly>
				</div>

        <div class="form-group">
				  <label>Cidade</label>
					<input type="text" class="form-control" value="{{ $registro->cidade }}" readonly>
				</div>

        <div class="form-group">
				  <label>Estado</label>
					<input type="text" class="form-control" value="{{ $registro->estado }}" readonly>
				</div>

        <div class="form-group">
				  <label>Telefone</label>
					<input type="text" class="form-control" value="{{ $registro->telefone }}" readonly>
				</div>

        <div class="form-group">
				  <label>E-mail</label>
					<input type="text" class="form-control" value="{{ $registro->email }}" readonly>
				</div>

        <div class="form-group">
				  <label>Data de Nascimento</label>
					<input type="text" class="form-control" value="{{ $registro->data_nascimento->format('d/m/Y') }}" readonly>
				</div>

        <div class="form-group">
				  <label>Sexo</label>
					<input type="text" class="form-control" value="{{ $registro->sexo }}" readonly>
				</div>

        <div class="form-group">
				  <label>Estabelecimento da Compra</label>
					<input type="text" class="form-control" value="{{ $registro->estabelecimento }}" readonly>
				</div>

        <div class="form-group">
				  <label>Cupom Fiscal</label>
					<input type="text" class="form-control" value="{{ $registro->cupom_fiscal }}" readonly>
				</div>

        <hr>

        <h3>Cupons</h3>

        @forelse($registro->cupons as $cupom)
          <div class="well">
            <strong>ID do Cupom:</strong> {{$cupom->hash}}<br>
            <strong>Código de Barra 1:</strong> {{$cupom->codigo_1}}<br>
            <strong>Código de Barra 2:</strong> {{$cupom->codigo_2}}<br>
            <strong>Código de Barra 3:</strong> {{$cupom->codigo_3}}
          </div>
        @empty
          <p>Nenhum cupom gerado</p>
        @endforelse

				<a href="{{ URL::route('painel.inscricoes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</div>
  </div>

@endsection
