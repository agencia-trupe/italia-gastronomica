<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="robots" content="index, nofollow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2013 Trupe Design" />
  <meta name="viewport" content="width=device-width,initial-scale=1">

	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<title>Promoção Itália Gastronômica</title>

	<base href="{{ base_url() }}/">
	<script>var BASE = "{{ base_url() }}"</script>

	<link href="{{ asset('/assets/css/painel.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<script src="assets/js/jquery.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	<script src='assets/js/modernizr.js'></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="body-painel">
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Promoção Itália Gastronômica</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

				@if(Auth::check())

					<ul class="nav navbar-nav">
						<li @if(str_is('*dashboard*', Route::CurrentRouteName())) class="active" @endif ><a href="{{ url('/painel') }}">Home</a></li>
						<li @if(str_is('*inscricoes*', Route::CurrentRouteName())) class="active" @endif ><a href="{{ url('/painel/inscricoes') }}">Inscrições</a></li>
						<li @if(str_is('*receitas*', Route::CurrentRouteName())) class="active" @endif><a href="{{ url('/painel/receitas') }}">Receitas</a></li>
						<li @if(str_is('*usuarios*', Route::CurrentRouteName())) class="active" @endif><a href="{{ URL::route('painel.usuarios.index') }}">Usuários do Painel</a></li>
					</ul>

					<ul class="nav navbar-nav navbar-right">
						<li><a href="{{ url('/painel/auth/logout') }}">Logout</a></li>
					</ul>

				@endif
			</div>
		</div>
	</nav>

	@yield('conteudo')

	<!-- Scripts -->
	<script src='assets/js/ckeditor/ckeditor.js'></script>
		<script src='assets/js/ckeditor/adapters/jquery.js'></script>
		<script src='assets/js/painel.js?nocache={{ time() }}'></script>

</body>
</html>
