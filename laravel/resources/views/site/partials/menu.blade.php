<header @if(str_is('home', Route::currentRouteName())) class="header-home" @endif >

  @if(str_is('home', Route::currentRouteName()))
    <img src="assets/img/titulo-italia-gastronomica-home.png" alt="Itália Gastronômica" id="imagem-titulo" />
  @endif

  <div id="midias-sociais">
    <a href="https://www.facebook.com/LaPastinaImportadora" target="_blank" title="Facebook" class="link-facebook"><img src="assets/img/ico-facebook.png" alt="Facebook" /></a>
    <a href="" target="_blank" title="Instagram" class="link-instagram"><img src="assets/img/ico-instagram.png" alt="Facebook" /></a>
  </div>

  <nav>
    <ul>
      <li>
        <a href="participe" title="PARTICIPE" @if(str_is('participe*', Route::currentRouteName())) class="ativo" @endif>PARTICIPE</a>
      </li>
      <li>
        <a href="regulamento" title="REGULAMENTO" @if(str_is('regulamento*', Route::currentRouteName())) class="ativo" @endif>REGULAMENTO</a>
      </li>
      <li>
        <a href="produtos" title="PRODUTOS" @if(str_is('produtos*', Route::currentRouteName())) class="ativo" @endif>PRODUTOS</a>
      </li>
      <li>
        <a href="premio" title="PRÊMIO" @if(str_is('premio*', Route::currentRouteName())) class="ativo" @endif>PRÊMIO</a>
      </li>
      <li>
        <a href="receitas" title="RECEITAS" @if(str_is('receitas*', Route::currentRouteName())) class="ativo" @endif>RECEITAS</a>
      </li>
    </ul>
  </nav>

</header>
