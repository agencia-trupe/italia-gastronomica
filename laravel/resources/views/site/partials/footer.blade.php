<footer>
  @if(str_is('home', Route::currentRouteName()))
    <img src="assets/img/imgs-base-home.png" id="imagem-footer" alt="Itália gastronômica" />
  @else
    <img src="assets/img/imgs-base-internas.png" id="imagem-footer" alt="Itália gastronômica" />
  @endif
  <p>
    PROMOÇÃO VIAGEM PELA ITÁLICA GASTRONÔMICA COM LA PASTINA. CERTIFICADO DE AUTORIZAÇÃO CAIXA Nº 6-2501/2014. IMAGENS MERAMENTE ILUSTRATIVAS. PERÍODO DA PROMOÇÃO DE 01/10/2016 A 30/11/2016.
  </p>
</footer>
