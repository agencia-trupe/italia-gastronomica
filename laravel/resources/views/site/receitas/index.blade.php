@extends('site.template')

@section('conteudo')
  <section class="section-receitas">

    <div class="chamada">
      <p>
        Confira nossas deliciosas sugestões de receitas com os produtos La Pastina
        e transforme sua cozinha em um espaço gourmet para sua família e amigos!
      </p>
    </div>

    <div class="lista-receitas">
      <ul>

        @if(sizeof($receitas))
          @foreach($receitas as $receita)
            <li>
              <a href="receitas/{{$receita->slug}}" title="{{$receita->titulo}}">
                <img src="assets/img/receitas/thumbs/{{$receita->thumb}}" alt="{{$receita->titulo}}" />
                {{$receita->titulo}}
              </a>
            </li>
          @endforeach
        @endif

      </ul>
    </div>

  </section>
@stop
