<section class="section-receitas">
  <div class="centralizar-reduzido">
    <div class="detalhes">
      <h1>{{$receita->titulo}}</h1>

      <div class="imagem">
        @if($receita->thumb)
          <img src="assets/img/receitas/{{$receita->thumb}}" alt="{{$receita->titulo}}" />
        @endif
        <img src="assets/img/marca-lapastina.png" alt="La Pastina" />
      </div>
      <div class="texto">

        <div class="ingredientes">
          <h2>Ingredientes</h2>
          {!! $receita->ingredientes !!}
        </div>

        <div class="preparo">
          <h2>Modo de Preparo</h2>
          {!! $receita->preparo !!}
        </div>

        <p>
          Tempo de preparo: {{ $receita->tempo_preparo }}
        </p>

        <p>
          Rendimento: {{ $receita->rendimento  }}
        </p>

        <a href="#" id="link-imprimir" title="Imprimir esta receita">
          <img src="assets/img/impressora.png" alt="Imprimir" />
        </a>

      </div>

    </div>
  </div>
</section>
