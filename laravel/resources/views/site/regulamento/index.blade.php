@extends('site.template')

@section('regulamento')
  <div class="regulamento">

    @if($isAjax)
      <div class="centralizar-reduzido">
    @endif

      <h1>REGULAMENTO</h1>

      <p>
        1. Promoção válida nos estados de São Paulo, Minas Gerais, Rio de Janeiro, Santa Catarina, Curitiba, Espirito Santo, Pará, Rio Grande do Norte, Rio Grande do Sul, Bahia e Distrito Federal, no período de 01/10/2016 à 30/11/2016, com apuração em 09/12/2016 realizada pela World Wine Comercial Bossa Nova Eireli (Mandatária), com sede na Praça Senador Salgado Filho, 1 Bloco D – Área 1 – Loja 1008B – Centro – Rio de Janeiro/RJ, inscrita no CNPJ nº 23.693.612/0001-12 e Real Comercial Ltda. (Aderente), com sede na Av. Presidente Wilson, 1786 – São Paulo/SP, CNPJ nº 02.780.640/0001-97.
      </p>
      <p>
        2. Qualquer pessoa física maior de 18 anos, residente e domiciliada nos estados de São Paulo, Minas Gerais, Rio de Janeiro, Santa Catarina, Curitiba, Espirito Santo, Pará, Rio Grande do Norte, Rio Grande do Sul, Bahia e Distrito Federal, poderá participar da presente promoção, promovida pela marca La Pastina, no período de 01/10/2016 à 30/11/2016.
      </p>
      <p>
        3. Para participar da promoção <strong>“Itália Gastronômica”</strong>, o consumidor que adquirir 03 (três) produtos diferentes da marca La Pastina participantes da promoção, na mesma nota fiscal no período de 01/10/2016 a 30/11/2016, terá direito se cadastrar no hotsite da promoção, para ter direito a 01 (um) cupom de participação.
      </p>
      <p>
        4. O consumidor que adquirir 03 (três) produtos diferentes da marca La Pastina participantes da promoção, poderá se inscrever na promoção acessando o site www.italiagastronomica.com.br, no período de 08h00 do dia 01/10/2016 a 23h59 do dia 30/11/2016 com seus dados pessoais (Nome Completo, CPF, Data de Nascimento, Endereço Completo, Bairro, Cidade, Estado CEP, Telefone, E-mail e Sexo) cadastrar os dados da compra (código de barras dos produtos La Pastina, nota fiscal da compra e loja onde comprou) e responder à pergunta: “<strong>Qual é a marca que leva você para a Itália?</strong>”.
        <br>
        <strong>Resposta: (   ) La Pastina		(   ) Outros</strong>
      </p>
      <p>
        4.1. Após a inscrição, os participantes não precisarão adotar nenhum procedimento, desta forma será emitido um cupom pela empresa promotora e colocado em urna para a apuração.<br>
        Obs.: O participante receberá a confirmação de sua inscrição na finalização do cadastro com a seguinte mensagem “Inscrição efetuada com sucesso”.
      </p>
      <p>
        4.2. Cada cupom/nota fiscal poderá ser cadastrado uma única vez na promoção, ou seja, caso o cliente realize a compra de seis ou mais produtos diferentes La Pastina em uma única nota fiscal, o mesmo deverá cadastrar de uma única vez todos os produtos La Pastina adquiridos juntamente com a nota fiscal correspondente.
      </p>
      <p>
        4.4. O participante inscrito nesta Promoção deverá armazenar/guardar o(s) cupom(ns)/nota(s) fiscal(is) referente(s) à aquisição dos produtos participantes adquiridos e cadastrados durante o período de participação até o término do período de divulgação do contemplado, sendo que, o contemplado deverá, após a apuração, apresentar à Empresa Promotora o(s) referido(s) documento(s), dentro do período de participação correspondente, para validação da condição de contemplado, sob pena de desclassificação. Nessa hipótese, o valor integral do prêmio será recolhido aos cofres da União, como prêmio prescrito.
      </p>
      <p>
        5. Não terão validade inscrições que não preencherem as condições básicas do concurso e que impossibilitarem a verificação de sua autenticidade.
      </p>
      <p>
        6. Não poderão participar do concurso funcionários da La Pastina, World Wine Comercial Bossa Nova Eireli, Real Comercial e funcionários de agências de propaganda e promoção diretamente envolvidas nesta promoção. O cumprimento desta cláusula será de responsabilidade da empresa promotora, através de consulta ao banco de dados de funcionários no momento da apuração.
      </p>
      <p>
        7. Não poderão participar da promoção, menores de 18 anos de idade.
      </p>
      <p>
        8. Os prêmios são intransferíveis e não poderão ser convertidos em dinheiro, nem ser trocados por outro(s) produto(s).
      </p>
      <p>
        9. <u>Forma de apuração:</u> Todos os cupons depositados em urnas e impressos pela empresa promotora serão encaminhados para o local da apuração e colocados em uma única urna. Dessa urna serão retirados, aleatoriamente, tantos cupons quantos forem necessários até que se encontre um cupom devidamente preenchido e com a resposta certa em igualdade com o número de prêmios a distribuir na apuração.
      </p>
      <p>
        10. Premiação: 01 (um) Pacote de viagem CVC (Itália Artística) para Itália, com direito a um acompanhante, incluindo passagens aéreas de ida/volta, 07 noites de hospedagem em hotel categoria turística, translado, seguro viagem e city tour.<br>
        Prêmio no valor de R$ 12.655,00.
      </p>
      <p>
        10.1. Condições gerais da viagem:
        <br>
        - O período de fruição da viagem será de 180 (cento e oitenta) dias, contados da assinatura da carta compromisso, deverá ser utilizada em baixa temporada e deverá ser agendada até 30 dias antes da viagem.
        <br>
        - A reserva de data do pacote de viagem deverá ser realizada com no mínimo 30 dias de antecedência da data de embarque, estando sujeita a disponibilidade de pacotes Itália Artística.
        <br>
        - Uma vez definida e reservada de pacote da viagem, não será permitido a remarcação e/ou alteração no pacote de viagem.
        <br>
        - Os documentos pessoais do contemplado como passaporte e vistos necessários para a realização da viagem será de responsabilidade do contemplado.
        <br>
        - A obtenção de passaporte devidamente válido, juntamente com o respectivo visto que viabilize a entrada do Contemplado e de seu Acompanhante em território estrangeiro será de inteira responsabilidade do Contemplado e de seu Acompanhante, devendo os mesmos arcar com todo e qualquer custo relacionado à obtenção de tais documentos, não podendo sua falta justificar qualquer dificuldade ou impedimento de participar da viagem. No caso de não obtenção/apresentação da documentação necessária à fruição do prêmio ou qualquer fator que impeça o usufruto do prêmio no período determinado para a viagem implicará na perda do direito ao prêmio pelo contemplado.
        <br>
        - A empresa não se responsabilizará pelas despesas pessoais realizadas pelo contemplado durante a viagem, tais como: despesas referentes à lavanderia, dispêndios pessoais (aquisição de bens em tour de compras, se for o caso), artigos de toucador, exceto os fornecidos pelo hotel em que ficará hospedado, telefonemas internacionais e locais, souvenires, consumo de itens de frigobar, excesso de bagagem ou seja, quaisquer outras despesas pessoais não previstas no pacote turístico serão de responsabilidade do contemplado.
        <br>
        - Caso o contemplado opte por um pacote de viagem Itália Artística de menor valor que o estipulado (R$ 12.655,00), não haverá reembolso. Caso o contemplado opte por um pacote de viagem Itália Artística de maior valor ou em outra data, o mesmo deverá completar a diferença.
        <br>
        - A empresa não assume qualquer responsabilidade por eventuais acidentes ou ocorrências resultantes de força maior que fujam totalmente de seu controle e que venham a causar danos materiais ou físicos ao contemplado e seu acompanhante durante a viagem.
        <br>
        - O contemplado receberá, no endereço fornecido em cadastro, ou na Administração da empresa, de forma gratuita e sem ônus, uma Carta Compromisso de entrega do prêmio que detalhará as informações para recebimento do pacote turístico, em até 30 dias corridos, contados da data de apuração do resultado. No momento da entrega da Carta Compromisso, contemplado deverá assinar um Termo de Quitação e Entrega de Prêmio e entregar uma cópia simples do seu RG e CPF/MF à empresa promotora.
        <br>
        - A empresa não se responsabilizará por furtos/roubos de bens de propriedade do contemplado que eventualmente ocorram durante a viagem, sendo que o vínculo da empresa promotora com o contemplado se encerrará a partir do momento da entrega da Carta Compromisso.
      </p>
      <p>
        11. <u>Data da apuração:</u> 09/12/2016 para cupons gerados de 01/10/2016 à 30/11/2016.
      </p>
      <p>
        12. <u>Local da apuração:</u> Escritório Administrativo á Avenida Presidente Wilson, 1786 e 1866 - São Paulo/SP, nas datas previstas, às 15:00 horas, com livre acesso aos interessados.
      </p>
      <p>
        13. <u>Exibição do prêmio:</u> Devido à natureza do prêmio o mesmo não poderá ser exibido. A empresa comprovará a propriedade dos prêmios até 8 (oito) dias antes da data marcada para realização do concurso de acordo com o Decreto 70951/72 – Art 15 – parágrafo 1º.
      </p>
      <p>
        14. <u>Entrega do prêmio:</u> Escritório Administrativo á Avenida Presidente Wilson, 1786 e 1866 - São Paulo/SP ou ainda no domicilio do contemplado até 30 dias a contar da data da apuração de acordo com o Decreto 70951/72 – Art 5º.
        <br>
        O prêmio distribuído deverá ser livre e desembaraçado de qualquer ônus para o contemplado.
      </p>
      <p>
        15. <u>Prescrição do direito aos prêmios:</u> 180 (cento e oitenta) dias após a data de cada apuração e os prêmios ganhos e não reclamados reverterão como Renda da União de acordo com o Art. 6º do Decreto 70951/72.
      </p>
      <p>
        16. <u>Divulgação da promoção:</u> Materiais de ponto de venda, anúncios impressos e internet. O regulamento completo da promoção estará disponibilizado na sede da empresa e no site www.italiagastronomica.com.br. O contemplado será avisado através de telefone e/ou telegrama e, desde já, autoriza a utilização de seu nome, imagem e som de voz, na divulgação do resultado da promoção sem qualquer ônus para a empresa promotora pelo período de até 01 ano a contar da data da apuração.
      </p>
      <p>
        17. As dúvidas e controvérsias oriundas de reclamações dos consumidores participantes da promoção deverão ser preliminarmente dirimidas pelos seus respectivos organizadores e, posteriormente, submetidas à CAIXA/REPCO.
        <br>
        O PROCON local, bem como os órgãos conveniados, em cada jurisdição receberão as reclamações devidamente fundamentadas, dos consumidores participantes.
      </p>
      <p>
        18. Fica, desde já, eleito o foro central da Comarca do São Paulo para solução de quaisquer questões referentes ao Regulamento da presente promoção.
      </p>
      <p>
        <strong>19. Certificado Autorização Caixa nº. 6-2415/2016 – Distribuição Gratuita</strong>
      </p>


    @if($isAjax)
      </div>
    @endif

    @if(!$isAjax)
      <a href="participe" id="link-participar" title="PARTICIPAR">PARTICIPAR</a>
    @endif

  </div>
@stop


@section('conteudo')
  <section class="section-regulamento">

    @yield('regulamento')

  </section>
@stop
