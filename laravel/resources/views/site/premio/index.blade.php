@extends('site.template')

@section('conteudo')
  <section class="section-premio">

    <div class="detalhe-premio">
      <p>
        <br><br>
        Um mergulho na cultura<br>
        e gastronomia Italiana com La Pastina.<br>
      </p>
    </div>

    <div class="chamada">
      <p>
        Informações do prêmio - Premio é o pacote Itália Artistica da CVC
        <br>
        A Itália é, certamente, um dos países mais fascinantes da Europa, pois abriga destinos desejados por muitos viajantes como Roma, cidade que respira história e impressiona com suas construções milenares. Há algumas horas dali está a charmosa Florença, berço do movimento renascentista e abrigo de prédios medievais. A romântica Veneza, por sua vez, atrai curiosos em descobrir os segredos das gôndolas e os estreitos canais que unem suas ilhas.
      </p>
    </div>

    <a href="participe" id="link-participar" title="PARTICIPAR">PARTICIPAR</a>

  </section>
@stop
