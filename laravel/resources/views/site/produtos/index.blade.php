@extends('site.template')

@section('conteudo')
  <section class="section-produtos">

    <div class="chamada">
      <p>
        Confira os produtos participantes da promoção:
      </p>
    </div>


    <a href="http://www.lapastina.com/public/view/site/layouts/layout/media/Catalogo_LA_PASTINA_2016.pdf" target="_blank" title="Ver catálogo completo" id="link-catalogo-completo">
      <img src="assets/img/botao_catalogo_completo.png" alt="Ver catálogo completo" />
    </a>


    <div class="lista-produtos">
      <ul>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/1-acetos-e-vinagres.png" alt="La Pastina - ACETOS BALSÂMICOS" />
          </div>
          <div class="texto">
            <div class="titulo">
              ACETOS BALSÂMICOS E VINAGRES
            </div>
            <table>
              <tr><td>ACETO BALSÂMICO DI MODENA 250ml</td></tr>
              <tr><td>ACETO BALSÂMICO DI MODENA 500ml</td></tr>
              <tr><td>ACETO BALSÂMICO DI MODENA ORGÂNICO 250ml</td></tr>
              <tr><td>ACETO BALSÂMICO DI MODENA 1L</td></tr>
              <tr><td>VINAGRE DE MAÇA BCO LA PASTINA 	250 ml</td></tr>
              <tr><td>VINAGRE DE VINHO BRANCO LA PASTINA 	500 ml</td></tr>
              <tr><td>VINAGRE DE VINHO TINTO LA PASTINA 	500 ml</td></tr>
              <tr><td>VINAGRE ORGÂNICO DE VINHO TINTO LA PASTINA 	250 ml</td></tr>
              <tr><td>VINAGRE ORGÂNICO DE VINHO BRANCO LA PASTINA 	250 ml</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/2-arroz.png" alt="La Pastina - ARROZ" />
          </div>
          <div class="texto">
            <div class="titulo">
              ARROZ
            </div>
            <table>
              <tr><td>ARROZ  ARBÓRIO 1Kg</td></tr>
              <tr><td>ARROZ CARNAROLI 1Kg</td></tr>
              <tr><td>ARROZ NEGRO 500g</td></tr>
              <tr><td>ARROZ NEGRO 1Kg</td></tr>
              <tr><td>ARROZ PER RISOTTO  1Kg</td></tr>
              <tr><td>ARROZ VERMELHO 500g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/4-azeites.png" alt="La Pastina - AZEITES" />
          </div>
          <div class="texto">
            <div class="titulo">
              AZEITES
            </div>
            <table>
              <tr><td>AZEITE EXTRAVIRGEM 250ml</td></tr>
              <tr><td>AZEITE EXTRAVIRGEM 500ml</td></tr>
              <tr><td>AZEITES  EXTRAVIRGEM ORGÂNICO 250ml</td></tr>
              <tr><td>AZEITES  EXTRAVIRGEM ORGÂNICO 500ml</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/5-azeitonas.png" alt="La Pastina - AZEITONAS" />
          </div>
          <div class="texto">
            <div class="titulo">
              AZEITONAS
            </div>
            <table>
              <tr><td>AZEITONAS CHILENAS KALAMATA  PRETAS FATIADAS 175g</td></tr>
              <tr><td>AZEITONAS CHILENAS KALAMATA  PRETAS INTEIRAS 216g</td></tr>
              <tr><td>AZEITONAS CHILENAS KALAMATA  PRETAS SEM CAROÇO 175g</td></tr>
              <tr><td>AZEITONAS ESP GORDAL VERDE COM CAROÇO 465g</td></tr>
              <tr><td>AZEITONAS ESP HOJIBLANCA  VERDE FATIADA 160g</td></tr>
              <tr><td>AZEITONAS ESP HOJIBLANCA  VERDE SEM CAROÇO 150g</td></tr>
              <tr><td>AZEITONAS ESP MANZANILLA  VERDE RECHEADAS COM ANCHOVAS 195g</td></tr>
              <tr><td>AZEITONAS ESP HOJIBLANCA  VERDE RECHEADAS COM PIMENTÃO 200g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/6-biscoitos-salgados.png" alt="La Pastina - BISCOITOS SALGADOS" />
          </div>
          <div class="texto">
            <div class="titulo">
              BISCOITOS SALGADOS
            </div>
            <table>
              <tr><td>CROSTINI TRADICIONAL 200g</td></tr>
              <tr><td>CROSTINI SAL MARINHO ALECRIM 200g</td></tr>
              <tr><td>CROSTINI PARMESÃO E PECORINO 200g</td></tr>
              <tr><td>TARALLO ERVA DOCE 200g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/7-condimentos.png" alt="La Pastina - CONDIMENTOS" />
          </div>
          <div class="texto">
            <div class="titulo">
              CONDIMENTOS
            </div>
            <table>
              <tr><td>AÇAFRÃO EM PÓ  0,4g</td></tr>
              <tr><td>AÇAFRÃO EM ESTIGMAS 0,4g</td></tr>
              <tr><td>FUNGHI PORCINI SECCHI 10g</td></tr>
              <tr><td>FUNGHI PORCINI SECCHI  20g</td></tr>
              <tr><td>KETCHUP BALSÂMICO 300g</td></tr>
              <tr><td>PINOLI 20g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/8-linha-aperitivo.png" alt="La Pastina - LINHA APERTIVO" />
          </div>
          <div class="texto">
            <div class="titulo">
              LINHA APERITIVO
            </div>
            <table>
              <tr><td>ALHOS APERITIVOS 180g</td></tr>
              <tr><td>ALHO TEMPERADO 180g</td></tr>
              <tr><td>MIX DE VEGETAIS TEMPERADO 160g</td></tr>
              <tr><td>PEPININHOS NO AZEITE DE OLIVA 160g</td></tr>
              <tr><td>AZEITONA VERDE SEM CAROÇO TEMPERADA 160g</td></tr>
              <tr><td>AZEITONA VERDE TEMPERADA 160g</td></tr>
              <tr><td>AZEITONA PRETA TEMPERADA 160G</td></tr>
              <tr><td>CEBOLINHA AO PESTO 160g</td></tr>
              <tr><td>PIPARRA TEMPERADA 150g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/9-conservas.png" alt="La Pastina - CONSERVAS" />
          </div>
          <div class="texto">
            <div class="titulo">
              CONSERVAS
            </div>
            <table>
              <tr><td>ALCAPARRAS 60g</td></tr>
              <tr><td>ALCAPARRONES 180g</td></tr>
              <tr><td>CEBOLINHAS 180g</td></tr>
              <tr><td>TOMATES SECOS 130g</td></tr>
              <tr><td>CORAÇÃO DE ALCACHOFRA GRELHADA 165g</td></tr>
              <tr><td>CORAÇÃO DE ALCACHOFRA 165g</td></tr>
              <tr><td>ASPARGOS BRANCOS 205g</td></tr>
              <tr><td>ASPARGOS VERDES 185g</td></tr>
              <tr><td>ASPARGOS BRANCOS FINOS CROCANTES 155g</td></tr>
              <tr><td>ASPARGOS VERDES FINOS CROCANTES  155g</td></tr>
              <tr><td>PIMENTÃO AO FORNO 225g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/10-pomodori.png" alt="La Pastina - POMODORI" />
          </div>
          <div class="texto">
            <div class="titulo">
              POMODORI
            </div>
            <table>
              <tr><td>POMODORI CUBETTI 260g</td></tr>
              <tr><td>POMODORI PELATI 240g</td></tr>
              <tr><td>PASSATA 680g</td></tr>
              <tr><td>POMODORI PELATI ORGÂNICO 400g</td></tr>
              <tr><td>PASSATA ORGÂNICA 680g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/11-bruschettas.png" alt="La Pastina - BRUSCHETTAS" />
          </div>
          <div class="texto">
            <div class="titulo">
              BRUSCHETTAS
            </div>
            <table>
              <tr><td>BRUSCHETTA GOURMET DE AZEITONA PRETA 140g</td></tr>
              <tr><td>BRUSCHETTA GOURMET DE AZEITONA VERDE 140g</td></tr>
              <tr><td>BRUSCHETTA GOURMET DE ALCACHOFRA 140g</td></tr>
              <tr><td>BRUSCHETTA GOURMET DE TOMATE SECO 140g</td></tr>
              <tr><td>BRUSCHETTA DE ALCACHOFRA 280g</td></tr>
              <tr><td>BRUSCHETTA DE ALCACHOFRA E PIMENTÃO 280g</td></tr>
              <tr><td>BRUSCHETTA DE ASPARGOS VERDES 280g</td></tr>
              <tr><td>BRUSCHETTA DE PIMENTÃO 280g</td></tr>
              <tr><td>BRUSCHETTA DE PIMENTA JALAPEÑO E PIMENTÃO 280g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/12-linha-na-brasa.png" alt="La Pastina - LINHA NA BRASA" />
          </div>
          <div class="texto">
            <div class="titulo">
              LINHA NA BRASA
            </div>
            <table>
              <tr><td>ABOBRINHA NA BRASA 145g</td></tr>
              <tr><td>ALCACHOFRA NA BRASA 145g</td></tr>
              <tr><td>ASPARGOS VERDES NA BRASA 185g</td></tr>
              <tr><td>BERINJELA NA BRASA 145g</td></tr>
              <tr><td>MIX DE VEGETAIS NA BRASA 145g</td></tr>
              <tr><td>PIMENTÃO NA BRASA 180g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/13-cremas-de-balsamico.png" alt="La Pastina - CREMAS DE BALSÂMICO" />
          </div>
          <div class="texto">
            <div class="titulo">
              CREMAS DE BALSÂMICO
            </div>
            <table>
              <tr><td>CREMA BALSÂMICO TRADICIONAL 250ml</td></tr>
              <tr><td>CREMA BALSÂMICO MORANGO 250ml</td></tr>
              <tr><td>CREMA BALSÂMICO FIGO 250ml</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/14-doces.png" alt="La Pastina - DOCES" />
          </div>
          <div class="texto">
            <div class="titulo">
              DOCES
            </div>
            <table>
              <tr><td>PÊSSEGOS EM CALDA DIET 485g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/15-farinha.png" alt="La Pastina - FARINHA" />
          </div>
          <div class="texto">
            <div class="titulo">
              FARINHA
            </div>
            <table>
              <tr><td>COUSCOUS 500g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/16-massa.png" alt="La Pastina - MASSA" />
          </div>
          <div class="texto">
            <div class="titulo">
              MASSA
            </div>
            <table>
              <tr><td>GNOCCHI 500g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/17-molhos.png" alt="La Pastina - MOLHOS" />
          </div>
          <div class="texto">
            <div class="titulo">
              MOLHOS
            </div>
            <table>
              <tr><td>MOLHO ARRABBIATA 320g</td></tr>
              <tr><td>MOLHO AMATRICIANA 320g</td></tr>
              <tr><td>MOLHO Al BASÍLICO 320g</td></tr>
              <tr><td>MOLHO Al CARCIOFI 320g</td></tr>
              <tr><td>MOLHO AI FUNGHI 320g</td></tr>
              <tr><td>MOLHO ALLA BOLOGNESE 320g</td></tr>
              <tr><td>MOLHO ALLA PUTTANESCA 320g</td></tr>
              <tr><td>MOLHO ALLE OLIVE 320g</td></tr>
              <tr><td>MOLHO TRADICIONAL 320g</td></tr>
              <tr><td>MOLHO GORGONZOLA 320g</td></tr>
              <tr><td>MOLHO FUNGHI TRUFADO 320g</td></tr>
              <tr><td>MOLHO PESTO ALLA GENOVESE 180g</td></tr>
              <tr><td>MOLHO PESTO ROSSO 180g</td></tr>
              <tr><td>MOLHO PESTO CREMOSO 190g</td></tr>
              <tr><td>MOLHO PESTO ALLA GENOVESE TRUFADO 180g</td></tr>
              <tr><td>CREME QUATRO QUEIJOS TRUFADO 180g</td></tr>
              <tr><td>MOLHO IT PESTO ALLA GENOVESE LA PASTINA 02 EMBALAGENS 90G | 180g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/18-quinoa.png" alt="La Pastina - QUINOA" />
          </div>
          <div class="texto">
            <div class="titulo">
              QUINOA
            </div>
            <table>
              <tr><td>QUINOA MIX ASPARGO E PIMENTÃO 185g</td></tr>
              <tr><td>QUINOA MIX PALMITO, MANGA E PIMENTÃO 185g</td></tr>
              <tr><td>QUINOA MIX PIMENTÃO 185g</td></tr>
              <tr><td>QUINOA MIX ALCACHOFRA 185g</td></tr>
              <tr><td>QUINOA DOCE LA PASTINA MANGA ABACAXI MAMÃO 220g</td></tr>
              <tr><td>QUINOA DOCE LA PASTINA MANGA E MARACUJÁ 220g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/19-risoto-pronto.png" alt="La Pastina - RISOTOS PRONTOS" />
          </div>
          <div class="texto">
            <div class="titulo">
              RISOTOS PRONTOS
            </div>
            <table>
              <tr><td>RISOTO DE FUNGHI PORCINI 250g</td></tr>
              <tr><td>RISOTO DE TRUFA NEGRA 250g</td></tr>
              <tr><td>RISOTO DE AÇAFRÃO 250g</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/20-sucos.png" alt="La Pastina - SUCO" />
          </div>
          <div class="texto">
            <div class="titulo">
              SUCO
            </div>
            <table>
              <tr><td>SUCO LA PASTINA TOMATE 200ml</td></tr>
              <tr><td>SUCO LA PASTINA TOMATE 1 L</td></tr>
            </table>
          </div>
        </li>

        <li>
          <div class="imagem">
            <img src="assets/img/produtos/3-food-service.png" alt="La Pastina - FOOD SERVICE" />
          </div>
          <div class="texto">
            <div class="titulo">
              FOOD SERVICE
            </div>
            <table>
              <tr><td>AZEITE EXTRAVIRGEM 5,05L</td></tr>
              <tr><td>FUNGHI PORCINI SECCHI 250g </td></tr>
              <tr><td>CORAÇÃO DE ALCACHOFRA 1,35Kg</td></tr>
              <tr><td>CORAÇÃO DE ALCACHOFRA 1,55Kg</td></tr>
              <tr><td>FUNDO DE ALCACHOFRA 1,3Kg</td></tr>
              <tr><td>POMODORI CUBETTI 1,53Kg</td></tr>
              <tr><td>POMODORI PELATI 1,53Kg</td></tr>
              <tr><td>PASSATA BAG 10Kg</td></tr>
            </table>
          </div>
        </li>


      </ul>
    </div>

  </section>
@stop
