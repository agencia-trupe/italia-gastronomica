@extends('site.template')

@section('conteudo')
  <section class="section-participe">

    @if(Session::has('participacao_cadastrada'))

      <div class="cadastro-realizado">
        <img src="assets/img/aviaozinho-retornoformulario.png">
        <p>
          Inscrição efetuada com sucesso!
          <br>
          Você está participando com <strong>{{ session('nro_cupons_gerados') > 1 ? session('nro_cupons_gerados').' cupons' : session('nro_cupons_gerados').' cupom'}} .</strong>
          <br>
          Boa sorte!
        </p>
      </div>

    @else

      <div class="chamada">
        <p>
          Siga o passo a passo abaixo para participar.<br>
          Quanto mais produtos comprar, mais chances de ganhar!
        </p>
      </div>

      <div id="passo-a-passo">
        <div class="passo passo-1">
          <div class="imagem">
            <img src="assets/img/img-passo1-cupomfiscal.png" alt="Passo 1" />
          </div>
          <div class="texto">Compre três produtos La Pastina diferentes.</div>
        </div>
        <div class="passo passo-2">
          <div class="imagem">
            <img src="assets/img/img-passo2-cupomfiscal.png" alt="Passo 2" />
          </div>
          <div class="texto">Cadastre o cupom fiscal e códigos de barras. <small>(Guarde o cupom fiscal até o final da promoção)</small></div>
        </div>
        <div class="passo passo-3">
          <div class="imagem">
            <img src="assets/img/img-passo3-italia.png" alt="Passo 3" />
          </div>
          <div class="texto">Concorra a uma viagem pela Itália Gastronômica!</div>
        </div>
      </div>

      <div class="chamada">
        <p>
          Cadastre-se e participe!
        </p>
      </div>

      <form action="participar" method="post">

        <div class="fundo">

          <input type="hidden" name="_token" value="{!! csrf_token() !!}">

          @if (count($errors) > 0)
  					<div class="retorno-erro">
  						Não foi possível enviar sua participação.
              <br>
  						{{ $errors->first() }}
  					</div>
  				@endif

          <label class="vermelho"><span>RESPONDA A PERGUNTA</span></label>

          <div class="questao">QUAL É A MARCA QUE LEVA VOCÊ PARA A ITÁLIA?</div>

          <div class="opcoes">
            <label><input type="radio" name="resposta" value="LA PASTINA" required @if(old('resposta') == 'LA PASTINA') checked @endif > LA PASTINA</label>
            <label><input type="radio" name="resposta" value="OUTROS" @if(old('resposta') == 'OUTROS') checked @endif > OUTROS</label>
          </div>

          <label class="vermelho"><span>PREENCHA COM SEUS DADOS</span></label>

          <label id="input-nome">
            NOME <input type="text" name="nome" required value="{{old('nome')}}">
          </label>

          <label id="input-cpf">
            CPF <input type="text" name="cpf" required value="{{old('cpf')}}">
          </label>

          <label id="input-cep">
            CEP <input type="text" name="cep" required value="{{old('cep')}}">
          </label>

          <label id="input-endereco">
            ENDEREÇO <input type="text" name="endereco" required value="{{old('endereco')}}">
          </label>

          <label id="input-numero">
            Nº <input type="text" name="numero" required value="{{old('numero')}}">
          </label>

          <label id="input-complemento">
            COMPLEMENTO <input type="text" name="complemento" value="{{old('complemento')}}">
          </label>

          <label id="input-bairro">
            BAIRRO <input type="text" name="bairro" required value="{{old('bairro')}}">
          </label>

          <label id="input-cidade">
            CIDADE <input type="text" name="cidade" required value="{{old('cidade')}}">
          </label>

          <label id="input-estado">
            ESTADO <input type="text" name="estado" required value="{{old('estado')}}">
          </label>

          <label id="input-telefone">
            TEL <input type="text" name="telefone" required value="{{old('telefone')}}">
          </label>

          <label id="input-email">
            EMAIL <input type="email" name="email" required value="{{old('email')}}">
          </label>

          <label id="input-data-nascimento">
            <small>DATA DE<br>NASCIMENTO</small> <input type="text" name="data_nascimento" required value="{{old('data_nascimento')}}">
          </label>

          <span id="input-sexo">
            SEXO
            <label><input type="radio" name="sexo" value="M" required @if(old('sexo') == 'M') checked @endif > M</label>
            <label><input type="radio" name="sexo" value="F" @if(old('sexo') == 'F') checked @endif > F</label>
          </span>

          <label id="input-estabelecimento">
            <small>SUPERMERCADO OU ESTABELECIMENTO<br>ONDE COMPROU O PRODUTO</small> <input type="text" name="estabelecimento" required value="{{old('estabelecimento')}}">
          </label>

          <label id="input-cupom-fiscal">
            CUPOM FISCAL <input type="text" name="cupom_fiscal" required value="{{old('cupom_fiscal')}}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
          </label>

          <label class="aviso-vermelho">
            CADASTRE AO LADO OS NÚMEROS DE CÓDIGOS DE BARRA DE PELO MENOS 3 PRODUTOS LA PASTINA
          </label>

          <div class="codigos">
            <label id="input-codigo1">
              <span>PRODUTO 1</span> <input type="text" name="codigos[]" required value="{{old('codigos.0')}}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
            </label>
            <label id="input-codigo2">
              <span>PRODUTO 2</span> <input type="text" name="codigos[]" required value="{{old('codigos.1')}}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
            </label>
            <label id="input-codigo3">
              <span>PRODUTO 3</span> <input type="text" name="codigos[]" required value="{{old('codigos.2')}}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
            </label>

            @if(count(old('codigos')) > 3)
              @foreach(old('codigos') as $k => $cod)
                @if($k >= 3)
                  <label id="input-codigo{{$k + 1}}">
                    <span>PRODUTO {{$k + 1}}</span> <input type="text" name="codigos[]" value="{{ is_array($cod) ? '' : $cod}}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                  </label>
                @endif
              @endforeach
            @endif

          </div>

          <a href="#" id="btn-adicionar-produto">+ adicionar produto</a>

          <label id="input-aceite">
             <input type="checkbox" required name="aceite_regulamento" value="1" @if(old('aceite_regulamento') == '1') checked @endif > Li e aceito os termos do <a href="regulamento" id="link-regulamento" title="Regulamento">regulamento.</a>
          </label>

          <div class="lembrete-embalagem">
            <strong>IMPORTANTE:</strong> GUARDE AS EMBALAGENS DOS 3 PRODUTOS CADASTRADOS E O CUPOM FISCAL, POIS SERÁ NECESSÁRIA A APRESENTAÇÃO NO MOMENTO DA PREMIAÇÃO.
          </div>

        </div>

        <input type="submit" value="ENVIAR">

      </form>

    @endif

  </section>
@stop
