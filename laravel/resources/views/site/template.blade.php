<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2016 Trupe Design" />
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <meta name="keywords" content="" />

	<title>Promoção Itália Gastronômica</title>
	<meta name="description" content="">
	<meta property="og:title" content="Promoção Itália Gastronômica"/>
	<meta property="og:description" content=""/>

  <meta property="og:site_name" content=""/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ base_url() }}">
	<script>var BASE = "{{ base_url() }}"</script>

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald:400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="assets/css/vendor.css">

	<link rel="stylesheet" href="assets/css/site.css?cachebuster={{ time() }}">

	<script src="assets/js/jquery.js"></script>
	<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script> -->

</head>
<body>

	<div class="centralizar">
		@include('site.partials.menu')

	  @yield('conteudo')

	  @include('site.partials.footer')
	</div>

	<script src="assets/js/site.js?cachebuster={{ time() }}"></script>

	</body>
</html>
