function preenche_endereco(conteudo) {
  if (!("erro" in conteudo)) {
    $('#input-endereco input').val(conteudo.logradouro);
    $('#input-bairro input').val(conteudo.bairro);
    $('#input-cidade input').val(conteudo.localidade);
    $('#input-estado input').val(conteudo.uf);
  }else {
    alert("CEP não encontrado.");
  }
}

function ajustaAltura(){
  alturaViewport = $(window).height();
  alturaCabecalho = $('header').outerHeight(true);
  alturaRodape = $('footer').outerHeight(true) + 5;
  paddingsBody = 28;

  alturaMinima = alturaViewport - alturaCabecalho - alturaRodape - paddingsBody;
  $('section').css('min-height', alturaMinima);
}

$('document').ready( function(){

  $('body').imagesLoaded( function() {
    ajustaAltura();
  });

  $('#input-cpf input').mask('000.000.000-00', {
    reverse: true,
    clearIfNotMatch: true
  });

  $('#btn-adicionar-produto').click( function(e){
    e.preventDefault();

    var nro_inputs = $('.codigos label').length;
    var nro_novo_input = nro_inputs + 1;
    var template = "";

    template += "<label id='input-codigo"+nro_novo_input+"'>";
    template += "<span>PRODUTO "+nro_novo_input+"</span> <input type='text' name='codigos[]' onkeypress='return event.charCode >= 48 && event.charCode <= 57'>";
    template += "</label>";

    $('.codigos').append(template);
  });


  $('#input-cep input').mask('00000-000', {
    reverse: true,
    clearIfNotMatch: true,
    onComplete: function(cep) {
      $('#input-endereco input').val('Buscando endereço...');
      var script = document.createElement('script');
      script.src = '//viacep.com.br/ws/'+ cep + '/json/?callback=preenche_endereco';
      document.body.appendChild(script);
    },
  });

  $('#input-telefone input').mask('(00) 0000-00009', { clearIfNotMatch: true });

  $('#input-data-nascimento input').mask('00/00/0000', { clearIfNotMatch: true });

  $("#link-regulamento").fancybox({
    type: 'ajax',
    title: false,
  });

  $(".lista-receitas a").fancybox({
    type: 'ajax',
    title: false,
    beforeShow: function(){
      var win=null;
      var content = $('.fancybox-inner');
      $('#link-imprimir').bind("click", function(){
        win = window.open("width=200,height=200");
        self.focus();
        win.document.open();
        win.document.write('<'+'html'+'><'+'head'+'><'+'style'+'>');
        win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
        win.document.write('#link-imprimir { display:none; }');
        win.document.write('.imagem img { display:block; max-width: 30%; }');
        win.document.write('<'+'/'+'style'+'><'+'/'+'head'+'><'+'body'+'>');
        win.document.write(content.html());
        win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
        win.document.close();
        win.print();
        win.close();
      });
    }
  });

});
