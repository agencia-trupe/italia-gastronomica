<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuponsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cupons', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('participacoes_id')->unsigned();
			$table->foreign('participacoes_id')->references('id')->on('participacoes')->onDelete('cascade');

			$table->string('codigo_1');
			$table->string('codigo_2');
			$table->string('codigo_3');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cupons');
	}

}
