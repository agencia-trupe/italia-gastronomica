<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipacoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('participacoes', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('resposta');
			$table->string('nome');
			$table->string('cpf');

			$table->string('cep');
			$table->string('endereco');
			$table->string('numero');
			$table->string('complemento');
			$table->string('bairro');
			$table->string('cidade');
			$table->string('estado');

			$table->string('telefone');
			$table->string('email');
			$table->date('data_nascimento');

			$table->string('sexo', 1);
			$table->string('estabelecimento');
			$table->string('cupom_fiscal');
			$table->string('codigo_1');
			$table->string('codigo_2');
			$table->string('codigo_3');

			$table->integer('aceite_regulamento');

			$table->string('ip');
			$table->string('ua');

			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('participacoes');
	}

}
