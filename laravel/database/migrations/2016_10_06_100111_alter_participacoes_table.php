<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterParticipacoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('participacoes', function(Blueprint $table)
		{
			$table->dropColumn('codigo_1');
			$table->dropColumn('codigo_2');
			$table->dropColumn('codigo_3');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('participacoes', function(Blueprint $table)
		{
			$table->string('codigo_1')->after('cupom_fiscal');
			$table->string('codigo_2')->after('codigo_1');
			$table->string('codigo_3')->after('codigo_2');
		});
	}

}
