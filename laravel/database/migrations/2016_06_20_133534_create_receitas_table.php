<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceitasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receitas', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('titulo');
			$table->string('slug');
			$table->string('thumb');
			$table->text('ingredientes');
			$table->text('preparo');
			$table->string('tempo_preparo');
			$table->string('rendimento');
			$table->integer('ordem');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receitas');
	}

}
