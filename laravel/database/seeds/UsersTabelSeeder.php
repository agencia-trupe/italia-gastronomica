<?php

use Illuminate\Database\Seeder;

class UsersTabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->delete();

      DB::table('users')->insert(
        [
          'nome' => 'Trupe',
          'login' => 'trupe',
          'email' => 'contato@trupe.net',
          'password' => bcrypt('senhatrupe'),
        ]
      );
    }
}
