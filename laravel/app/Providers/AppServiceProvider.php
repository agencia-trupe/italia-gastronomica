<?php namespace ItaliaGastronomica\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use ItaliaGastronomica\Models\Participacao;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Validator::extend('validar_codigos', function($attribute, $value, $parameters, $validator) {
			// retornar true SE:

			// 1. houver pelo menos 3 códigos válidos
			// 2. Cod válido = não vazio, só digitos e existir na lista de códigos participantes

			$validos = [];

			if(!is_array($value)) return false;

			foreach($value as $item){
				if($item != '' && is_numeric($item) && verifica_cod_barra($item))
					array_push($validos, $item);
			}

			return count($validos) >= 3;
		});

		Validator::extend('minimo_um_cupom', function($attribute, $value, $parameters, $validator) {
			// retornar true SE for formado pelo menos 1 cupom com os codigos fornecidor
			// um cupom deve ter 3 códigos diferentes

			$cupons_validos = gerar_cupons($value);

			return count($cupons_validos) > 0;
		});

		Validator::extend('validar_cupom_fiscal', function($attribute, $value, $parameters, $validator) {
			// retornar true SE NÃO houver a combinação cupom_fiscal + estabelecimento na tabela de participacoes
			$cupom_fiscal = $value;
			$estabelecimento = $parameters[0];

			$check = Participacao::where('estabelecimento', $estabelecimento)->where('cupom_fiscal', $cupom_fiscal)->get();

			return count($check) == 0;
		});

	}


	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'ItaliaGastronomica\Services\Registrar'
		);
	}

}
