<?php

/**
 * Retorna a url base do projeto
 *
 * @return string
 */
if ( ! function_exists('base_url'))
{
	function base_url()
	{
		//return 'http://'.$_SERVER['HTTP_HOST'].'/previa-italia-gastronomica/';
		return 'http://'.$_SERVER['HTTP_HOST'];
	}
}


/**
 * Verifica se um código de barra é participante da promoção
 *
 * @param  string  $str
 * @return bool
 */
if ( ! function_exists('verifica_cod_barra'))
{
	function verifica_cod_barra($str)
	{
		$codigos_participantes = [
			'7896196082189', '7896196080307', '7896196062426', '7896196061825',
			'7896196080260', '7896196083179', '7896196060361', '7896196059211',
			'7896196061337', '7896196061344', '7896196082158', '7896196080017',
			'7896196061306', '7896196061733', '7896196061740', '7896196082448',
			'7896196058986', '7896196058979', '7896196058993', '7896196082431',
			'7896196081915', '7896196082417', '7896196081908', '7896196061474',
			'7896196061481', '7896196061498', '7896196061504', '7896196061313',
			'7896196061320', '8017465000041', '8017465000034', '8017465000072',
			'8022813000116', '7896196061801', '7896196083889', '7896196061160',
			'7896196060897', '7896196083872', '7896196061610', '7896196061627',
			'7896196061634', '7896196061641', '7896196061658', '7896196020013',
			'7896196062358', '7896196082622', '7896196080444', '7896196060552',
			'7896196080451', '7896196059044', '7896196061665', '7896196062334',
			'7896196062341', '7896196082608', '7896196082585', '7896196061122',
			'7896196061139', '7896196059440', '7896196062259', '7896196061382',
			'7896196061399', '7896196059464', '7896196060576', '7896196059129',
			'7896196062310', '7896196062327', '7896196061696', '7896196061702',
			'7896196061719', '7896196061726', '7896196060965', '7896196060996',
			'7896196060989', '7896196060972', '7896196061009', '7896196061023',
			'7896196059471', '7896196059495', '7896196061030', '7896196059488',
			'7896196059501', '7896196059518', '7896196062204', '7896196062211',
			'7896196062228', '7896196061436', '7896196061443', '7896196061450',
			'7896196082110', '7896196061467', '7896196082202', '7896196059525',
			'7896196061412', '7896196061429', '7896196080505', '7896196080543',
			'7896196080550', '7896196080581', '7896196080529', '7896196080536',
			'7896196080512', '7896196061405', '7896196061672', '7896196062402',
			'7896196059006', '7896196061016', '7896196062303', '7896196059013',
			'7896196059020', '7896196062266', '7896196062273', '7896196062280',
			'7896196062297', '7896196062471', '7896196062488', '7896196061351',
			'7896196061368', '7896196061375', '7896196059419', '7896196059426',
			'7896196083025', '7896196080291', '7896196080284', '7896196062433',
			'7896196062440'
		];
		return in_array($str, $codigos_participantes);
	}
}

/**
 * Gera os cupons da promoção
 *
 * @param  array  $arr_codigos
 * @return array
 */
if ( ! function_exists('gerar_cupons'))
{
	function gerar_cupons($arr_codigos)
	{
		/*
		* Cupons são trincas de códigos válidos não repetidos
		* Exemplo de cupom:
			$cupons = [
				['0004', '0005', '0006'],
				['0004', '0001', '0008'],
				['0004', '0001', '0458'],
			]
		*
		*/
		$cupons = [];

		if(!is_array($arr_codigos)) return false;

		foreach($arr_codigos as $cod){
			if($cod != '' && is_numeric($cod) && verifica_cod_barra($cod)){

				if(count($cupons) == 0){

					$novo_cupom = [];
					array_push($novo_cupom, $cod);
					array_push($cupons, $novo_cupom);

				}else{

					$nro_cupons = count($cupons);
					$adicionado_a_cupom_existente = false;

					for ($i=0; $i < $nro_cupons; $i++) {

						if(!in_array($cod, $cupons[$i]) && count($cupons[$i]) < 3){
							array_push($cupons[$i], $cod);
							$adicionado_a_cupom_existente = true;
							break;
						}

					}

					if(!$adicionado_a_cupom_existente){
						$novo_cupom = [];
						array_push($novo_cupom, $cod);
						array_push($cupons, $novo_cupom);
					}

				}
			}
		}

		// Remover cupons com menos de 3 códigos
		foreach($cupons as $k => $cupom){
			if(count($cupom) < 3){
				unset($cupons[$k]);
			}
		}

		return $cupons;
	}
}



/**
 * Limit the number of words in a string.
 *
 * @param  string  $value
 * @param  int     $words
 * @param  string  $end
 * @return string
 */
if ( ! function_exists('str_words'))
{
	function str_words($value, $words = 100, $end = '...')
	{
		preg_match('/^\s*+(?:\S++\s*+){1,'.$words.'}/u', $value, $matches);

		if ( ! isset($matches[0])) return $value;

		if (strlen($value) == strlen($matches[0])) return $value;

		return rtrim($matches[0]).$end;
	}
}

/**
 *
 * Simply adds the http:// part if no scheme is included
 *
 * @access	public
 * @param	string	$str
 * @return	string
 */
if ( ! function_exists('prep_url'))
{
	function prep_url($str = '')
	{
		if ($str == 'https://' OR $str == 'http://' OR $str == '')
		{
			return '';
		}

		$url = parse_url($str);

		if ( ! $url OR ! isset($url['scheme']))
		{
			$str = 'http://'.$str;
		}

		return $str;
	}
}

/**
 *
 * Generate embed code to a given 'google maps incorporation code'
 *
 * @access	public
 * @param	string	$str
 * @param	string  $width
 * @param	string  $height
 * @param	string  $classe
 * @return	string
 */
if ( ! function_exists('embed_maps'))
{
	function embed_maps($str = '', $width = '', $height = '', $classe = '')
	{

	    //$str = stripslashes(htmlspecialchars_decode($str));

	    if($width != '')
	        $str = preg_replace("~width=\"(\d+)\"~", 'width="'.$width.'"', $str);
	    else
	    	$str = preg_replace("~width=\"(\d+)\"~", '', $str);

	    if($height != '')
	        $str = preg_replace("~height=\"(\d+)\"~", 'height="'.$height.'"', $str);
	    else
	    	$str = preg_replace("~height=\"(\d+)\"~", '', $str);

	    if($classe != '')
	    	$str = preg_replace("~<iframe~", "<iframe class='{$classe}'", $str);

		// COM o link 'ver mapa ampliado'
	    // return $str;

	    // SEM o link 'ver mapa ampliado'
	    return preg_replace('~<br \/>(.*)~', '', $str);
	}
}

/**
 *
 * Separate the username from a Instagram URL
 *
 * @access	public
 * @param	string	$url
 * @return	string
 */
if ( ! function_exists('get_social_user'))
{
	function get_social_user($url = '', $prefixo = '@', $offset = 0)
	{
	    $username = '';

	    $parse = parse_url($url);

    	if($parse && isset($parse['host'])){

	    	if(strpos($parse['host'], 'tumblr') !== false){

	    		$xpd = explode('.', $parse['host']);
		    	$retorno = isset($xpd[0 + $offset]) ? $xpd[0 + $offset] : '';

	    	}elseif(isset($parse['path']) && $parse['path'] != '/'){

		    	$xpd = explode('/', $parse['path']);
		    	$retorno = isset($xpd[1 + $offset]) ? $xpd[1 + $offset] : '';

	    	}

	    	return $prefixo.$retorno;

	    }else{
	    	return '';
	    }
	}
}

/**
 *
 * Return youtube embed url based on a Youtube video id
 *
 * @access	public
 * @param		string	$url
 * @return	string
 */
if ( ! function_exists('youtube_embed_link'))
{
	function youtube_embed_link($video_id)
	{
	  return "<iframe src='http://www.youtube.com/embed/{$video_id}?autoplay=0' frameborder='0' allowfullscreen></iframe>";
	}
}

/**
 *
 * Return client IP
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('ip'))
{
	function ip(){
	  if(isset($_SERVER["REMOTE_ADDR"]))
	    return $_SERVER["REMOTE_ADDR"];
	  elseif(isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
	    return $_SERVER["HTTP_X_FORWARDED_FOR"];
		elseif(isset($_SERVER["HTTP_CLIENT_IP"]))
	    return $_SERVER["HTTP_CLIENT_IP"];
	}
}

/**
 *
 * Return client user agent
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('user_agent'))
{
	function user_agent(){
	  return isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
	}
}
