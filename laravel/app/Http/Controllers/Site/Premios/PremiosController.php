<?php

namespace ItaliaGastronomica\Http\Controllers\Site\Premios;

use Illuminate\Http\Request;
use ItaliaGastronomica\Http\Controllers\Controller;


class PremiosController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.premio.index');
  }

}
