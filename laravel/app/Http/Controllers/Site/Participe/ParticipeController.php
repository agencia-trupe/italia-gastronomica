<?php

namespace ItaliaGastronomica\Http\Controllers\Site\Participe;

use Illuminate\Http\Request;
use ItaliaGastronomica\Http\Controllers\Controller;

use ItaliaGastronomica\Models\Participacao;
use ItaliaGastronomica\Models\Cupom;

class ParticipeController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.participe.index');
  }

  public function postParticipar(Request $request)
  {
    $this->validate($request, [
      'resposta' => 'required|in:LA PASTINA,OUTROS',
      'nome'     => 'required',
      'cpf'      => 'required',
      'cep'      => 'required',
      'endereco' => 'required',
      'numero'   => 'required',
      'bairro'   => 'required',
      'cidade'   => 'required',
      'estado'   => 'required',
      'telefone' => 'required',
      'email'    => 'required|email',
      'data_nascimento' => 'required|date_format:d/m/Y',
      'sexo'            => 'required|in:M,F',
      'estabelecimento' => 'required',
      'cupom_fiscal'    => 'required|validar_cupom_fiscal:'.$request->estabelecimento,
      'aceite_regulamento' => 'required|in:1',
      'codigos'            => 'required|validar_codigos|minimo_um_cupom'
    ]);

    $participacao = Participacao::create($request->except('codigos'));
    $participacao->ip = ip();
    $participacao->ua = user_agent();
    $participacao->save();

    $cupons_validos = gerar_cupons($request->codigos);

    foreach($cupons_validos as $cupom){
      $novo_cupom = new Cupom;
      $novo_cupom->participacoes_id = $participacao->id;
      $novo_cupom->codigo_1 = $cupom[0];
      $novo_cupom->codigo_2 = $cupom[1];
      $novo_cupom->codigo_3 = $cupom[2];
      $novo_cupom->save();
    }

    $request->session()->flash('participacao_cadastrada', true);
    $request->session()->flash('nro_cupons_gerados', count($cupons_validos));
    return back();
  }

}
