<?php

namespace ItaliaGastronomica\Http\Controllers\Site\Regulamento;

use Illuminate\Http\Request;
use ItaliaGastronomica\Http\Controllers\Controller;


class RegulamentoController extends Controller
{

  public function getIndex(Request $request)
  {
    $view = view('site.regulamento.index')->with('isAjax', $request->ajax());

    if($request->ajax()) {
      $sections = $view->renderSections();
      return $sections['regulamento'];
    }

    return $view;
  }

}
