<?php

namespace ItaliaGastronomica\Http\Controllers\Site\Receitas;

use Illuminate\Http\Request;
use ItaliaGastronomica\Http\Controllers\Controller;

use ItaliaGastronomica\Models\Receita;

class ReceitasController extends Controller
{

  public function getIndex()
  {
    $receitas = Receita::ordenado()->get();
    return view('site.receitas.index', compact('receitas'));
  }

  public function getDetalhes($slug)
  {
    $receita = Receita::findBySlug($slug);
    return view('site.receitas.detalhes', compact('receita'));
  }

}
