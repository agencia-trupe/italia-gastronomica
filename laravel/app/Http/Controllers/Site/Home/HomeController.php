<?php

namespace ItaliaGastronomica\Http\Controllers\Site\Home;

use Illuminate\Http\Request;
use ItaliaGastronomica\Http\Controllers\Controller;


class HomeController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.home.index');
  }

}
