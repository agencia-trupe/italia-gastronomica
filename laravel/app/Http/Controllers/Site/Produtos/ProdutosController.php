<?php

namespace ItaliaGastronomica\Http\Controllers\Site\Produtos;

use Illuminate\Http\Request;
use ItaliaGastronomica\Http\Controllers\Controller;


class ProdutosController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('site.produtos.index');
  }

}
