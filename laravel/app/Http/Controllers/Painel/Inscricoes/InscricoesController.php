<?php

namespace ItaliaGastronomica\Http\Controllers\Painel\Inscricoes;

use Illuminate\Http\Request;
use ItaliaGastronomica\Http\Controllers\Controller;

use ItaliaGastronomica\Models\Participacao;

class InscricoesController extends Controller{

	public function getIndex()
	{
		$registros = Participacao::all();

		return view('painel.inscricoes.index')->with(compact('registros'));
	}

  public function getDetalhes($participacao_id){
    $registro = Participacao::find($participacao_id);
    return view('painel.inscricoes.show')->with(compact('registro'));
  }

	public function postDestroy(Request $request, $id){
		$object = Participacao::find($id);
		$object->delete();

		$request->session()->flash('sucesso', 'Participação removida com sucesso.');

		return redirect()->route('painel.inscricoes.index');
	}

  public function getDownload(Request $request){
    $registros = Participacao::orderBy('cpf')->get();

    $filename = 'Cadastros_Promocao_Italia_Gastronomica_'.date('d_m_Y');

    $registrosArray = [];
		$linha = 0;		

    foreach ($registros as $key => $value) {
			if(count($value->cupons) > 0){
				foreach ($value->cupons as $cupom_index => $cupom) {
		      $registrosArray[$linha]['nome'] = $value->nome;
		      $registrosArray[$linha]['email'] = $value->email;
		      $registrosArray[$linha]['data de cadastro'] = $value->created_at->format('d/m/Y H:i');
		      $registrosArray[$linha]['cpf'] = $value->cpf;
		      $registrosArray[$linha]['cep'] = $value->cep;
		      $registrosArray[$linha]['endereco'] = $value->endereco;
		      $registrosArray[$linha]['numero'] = $value->numero;
		      $registrosArray[$linha]['complemento'] = $value->complemento;
		      $registrosArray[$linha]['bairro'] = $value->bairro;
		      $registrosArray[$linha]['cidade'] = $value->cidade;
		      $registrosArray[$linha]['estado'] = $value->estado;
		      $registrosArray[$linha]['telefone'] = $value->telefone;
		      $registrosArray[$linha]['data_nascimento'] = $value->data_nascimento->format('d/m/Y H:i');
		      $registrosArray[$linha]['sexo'] = $value->sexo;
		      $registrosArray[$linha]['estabelecimento'] = $value->estabelecimento;
		      $registrosArray[$linha]['cupom_fiscal'] = $value->cupom_fiscal;
		      $registrosArray[$linha]['codigo_1'] = $cupom->codigo_1;
		      $registrosArray[$linha]['codigo_2'] = $cupom->codigo_2;
		      $registrosArray[$linha]['codigo_3'] = $cupom->codigo_3;
					$linha++;
				}
			}
    }

    \Excel::create($filename, function($excel) use($registrosArray) {
	    $excel->sheet('Relatório de Cadastros', function($sheet) use ($registrosArray) {
	      $sheet->fromArray($registrosArray);
	    });
		})->download('xls');
  }

}
