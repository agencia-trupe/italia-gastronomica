<?php

namespace ItaliaGastronomica\Http\Controllers\Painel\Receitas;

use Illuminate\Http\Request;
use ItaliaGastronomica\Http\Controllers\Controller;
use ItaliaGastronomica\Libs\Thumbs;

use ItaliaGastronomica\Models\Receita;

class ReceitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Receita::ordenado()->get();

      return view('painel.receitas.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.receitas.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:receitas,titulo',
        'imagem' => 'required|image'
    	]);

      $object = new Receita;

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->ingredientes = $request->ingredientes;
      $object->preparo = $request->preparo;
      $object->tempo_preparo = $request->tempo_preparo;
      $object->rendimento = $request->rendimento;

      $imagem = Thumbs::make($request, 'imagem', 600, null, 'receitas/');
      if($imagem){
        $object->thumb = $imagem;
        Thumbs::makeFromFile('assets/img/receitas/', $imagem, 200, 200, 'assets/img/receitas/thumbs/');
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.receitas.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.receitas.edit')->with('registro', Receita::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:receitas,titulo,'.$id,
        'imagem' => 'sometimes|image'
    	]);

      $object = Receita::find($id);

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->ingredientes = $request->ingredientes;
      $object->preparo = $request->preparo;
      $object->tempo_preparo = $request->tempo_preparo;
      $object->rendimento = $request->rendimento;

      $imagem = Thumbs::make($request, 'imagem', 200, 200, 'receitas/');
      if($imagem){
        $object->thumb = $imagem;
        Thumbs::makeFromFile('assets/img/receitas/', $imagem, 200, 200, 'assets/img/receitas/thumbs/');
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.receitas.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Receita::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.receitas.index');
    }

}
