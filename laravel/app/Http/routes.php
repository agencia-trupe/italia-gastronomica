<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group([
  'namespace' => 'Site'
], function(){

  Route::get('/', ['as' => 'home', 'uses' => 'Home\HomeController@getIndex']);
  Route::get('participe', ['as' => 'participe', 'uses' => 'Participe\ParticipeController@getIndex']);
  Route::post('participar', ['as' => 'participar', 'uses' => 'Participe\ParticipeController@postParticipar']);
  Route::get('regulamento', ['as' => 'regulamento', 'uses' => 'Regulamento\RegulamentoController@getIndex']);
  Route::get('produtos', ['as' => 'produtos', 'uses' => 'Produtos\ProdutosController@getIndex']);
  Route::get('premio', ['as' => 'premio', 'uses' => 'Premios\PremiosController@getIndex']);
  Route::get('receitas', ['as' => 'receitas', 'uses' => 'Receitas\ReceitasController@getIndex']);
  Route::get('receitas/{slug}', ['as' => 'receitas.detalhes', 'uses' => 'Receitas\ReceitasController@getDetalhes']);


});


// Authentication routes
Route::get('painel/auth/login', ['as' => 'painel.login','uses' => 'Painel\Auth\AuthController@getLogin']);
Route::post('painel/auth/login', ['as' => 'painel.auth','uses' => 'Painel\Auth\AuthController@postLogin']);
Route::get('painel/auth/logout', ['as' => 'painel.logout','uses' => 'Painel\Auth\AuthController@getLogout']);

Route::group([
  'middleware' => 'auth',
  'namespace' => 'Painel',
  'prefix' => 'painel'
], function() {

  Route::get('/', ['as' => 'painel.dashboard', 'uses' => function() {
    return view('painel.dashboard.index');
  }]);

  Route::post('gravar-ordem-registros', function(Illuminate\Http\Request $request){
    $itens = $request->input('data');
    $tabela = $request->input('tabela');
    for ($i = 0; $i < count($itens); $i++)
	    DB::table($tabela)->where('id', $itens[$i])->update(array('ordem' => $i));
  });

  Route::resource('usuarios', 'Usuarios\UsuariosController');
  Route::resource('receitas', 'Receitas\ReceitasController', ['except' => ['show']]);
  Route::get('inscricoes', ['as' => 'painel.inscricoes.index', 'uses' => 'Inscricoes\InscricoesController@getIndex']);
  Route::get('inscricoes/detalhes/{id}', ['as' => 'painel.inscricoes.show', 'uses' => 'Inscricoes\InscricoesController@getDetalhes']);
  Route::post('inscricoes/remover/{id}', ['as' => 'painel.inscricoes.destroy', 'uses' => 'Inscricoes\InscricoesController@postDestroy']);
  Route::get('download-inscricoes', ['as' => 'painel.inscricoes.download', 'uses' => 'Inscricoes\InscricoesController@getDownload']);


});
