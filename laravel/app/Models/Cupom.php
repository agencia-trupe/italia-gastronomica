<?php

namespace ItaliaGastronomica\Models;

use Illuminate\Database\Eloquent\Model;

class Cupom extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cupons';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
    'participacoes_id',
    'codigo_1',
    'codigo_2',
    'codigo_3'
  ];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['id'];

	public function getHashAttribute()
	{
			return str_pad($this->id, 8, '0', STR_PAD_LEFT);
	}

	public function participacao()
  {
    return $this->belongsTo('ItaliaGastronomica\Models\Participacao', 'participacoes_id');
  }
}
