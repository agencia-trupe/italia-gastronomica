<?php

namespace ItaliaGastronomica\Models;

use Illuminate\Database\Eloquent\Model;

class Receita extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'receitas';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
    'titulo',
    'slug',
    'thumb',
    'ingredientes',
    'preparo',
    'tempo_preparo',
    'rendimento',
    'ordem'
  ];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['id'];

	public function scopeOrdenado($query)
	{
		return $query->orderBy('ordem', 'asc');
	}

	public function scopeFindBySlug($query, $slug)
	{
		return $query->where('slug', $slug)->firstOrFail();
	}
}
