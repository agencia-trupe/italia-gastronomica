<?php

namespace ItaliaGastronomica\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon as Carbon;

class Participacao extends Model {

	 use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'participacoes';

	protected $dates = [
		'data_nascimento',
		'created_at',
		'updated_at',
		'deleted_at'
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
    'resposta',
    'nome',
    'cpf',
    'cep',
    'endereco',
    'numero',
    'complemento',
    'bairro',
    'cidade',
    'estado',
    'telefone',
    'email',
		'sexo',
    'data_nascimento',
    'estabelecimento',
    'cupom_fiscal',
    'codigo_1',
    'codigo_2',
    'codigo_3',
    'aceite_regulamento'
    // 'ip',
    // 'ua',
  ];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['id'];

	public function setDataNascimentoAttribute($valor) {
    $valor_carbon = Carbon::createFromFormat('d/m/Y', $valor);
    return $this->attributes['data_nascimento'] = $valor_carbon->format('Y-m-d');
  }

	public function cupons()
	{
		return $this->hasMany('ItaliaGastronomica\Models\Cupom', 'participacoes_id')->orderBy('id', 'asc');
	}
}
