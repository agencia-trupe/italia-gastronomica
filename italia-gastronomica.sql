-- phpMyAdmin SQL Dump
-- version 4.0.10.12
-- http://www.phpmyadmin.net
--
-- Máquina: 187.45.196.241
-- Data de Criação: 13-Out-2016 às 18:39
-- Versão do servidor: 5.1.54-rel12.6-log
-- versão do PHP: 5.6.24-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `trupe114`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cupons`
--

CREATE TABLE IF NOT EXISTS `cupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `participacoes_id` int(10) unsigned NOT NULL,
  `codigo_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `cupons_participacoes_id_foreign` (`participacoes_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `cupons`
--

INSERT INTO `cupons` (`id`, `participacoes_id`, `codigo_1`, `codigo_2`, `codigo_3`, `created_at`, `updated_at`) VALUES
(1, 2, '7896196082189', '7896196082189', '7896196080260', '2016-10-10 19:09:24', '2016-10-10 19:09:24'),
(2, 3, '7896196061337', '7896196061337', '7896196061337', '2016-10-10 19:11:34', '2016-10-10 19:11:34'),
(3, 4, '7896196082189', '7896196080307', '7896196062426', '2016-10-10 19:14:00', '2016-10-10 19:14:00'),
(4, 4, '7896196080260', '7896196083179', '7896196083179', '2016-10-10 19:14:00', '2016-10-10 19:14:00'),
(5, 5, '7896196083179', '7896196083179', '7896196083179', '2016-10-10 19:31:17', '2016-10-10 19:31:17'),
(6, 5, '7896196083179', '7896196083179', '7896196083179', '2016-10-10 19:31:17', '2016-10-10 19:31:17'),
(7, 5, '7896196083179', '7896196083179', '7896196083179', '2016-10-10 19:31:17', '2016-10-10 19:31:17'),
(8, 6, '8435261900424', '7896196061351', '7896196059419', '2016-10-10 19:36:58', '2016-10-10 19:36:58');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2016_06_20_133534_create_receitas_table', 1),
('2016_06_20_133544_create_participacoes_table', 1),
('2016_10_06_095359_create_cupons_table', 2),
('2016_10_06_100111_alter_participacoes_table', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `participacoes`
--

CREATE TABLE IF NOT EXISTS `participacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `resposta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `numero` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `complemento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_nascimento` date NOT NULL,
  `sexo` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `estabelecimento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cupom_fiscal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `aceite_regulamento` int(11) NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ua` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `participacoes`
--

INSERT INTO `participacoes` (`id`, `resposta`, `nome`, `cpf`, `cep`, `endereco`, `numero`, `complemento`, `bairro`, `cidade`, `estado`, `telefone`, `email`, `data_nascimento`, `sexo`, `estabelecimento`, `cupom_fiscal`, `aceite_regulamento`, `ip`, `ua`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'LA PASTINA', 'Teste', '476.656.565-65', '04815-330', 'Rua Oswaldo Diniz', '454', '', 'Jardim Satélite', 'São Paulo', 'SP', '(11) 11111-1111', 'email@email.com', '2998-12-03', 'M', 'Teste', '000', 1, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36', '2016-06-22 16:48:05', '2016-06-22 14:06:46', '2016-06-22 16:48:05'),
(2, 'LA PASTINA', 'LILIANE ROSA', '278.819.568-01', '01426-020', 'Rua Cristóvão Diniz', '67', '', 'Cerqueira César', 'São Paulo', 'SP', '(11) 9893-22771', 'liliane.rosa@lapastina.com', '1980-07-07', 'F', 'EAT', '203110', 1, '189.51.11.226', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36', NULL, '2016-10-10 19:09:24', '2016-10-10 19:09:24'),
(3, 'LA PASTINA', 'LILIANE ROSA', '278.819.568-01', '01426-020', 'Rua Cristóvão Diniz', '67', '', 'Cerqueira César', 'São Paulo', 'SP', '(11) 9893-22771', 'liliane.rosa@lapastina.com', '1980-07-07', 'F', 'EAT', '203111', 1, '189.51.11.226', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36', NULL, '2016-10-10 19:11:34', '2016-10-10 19:11:34'),
(4, 'LA PASTINA', 'LILIANE ROSA', '278.819.568-01', '01426-020', 'Rua Cristóvão Diniz', '67', '', 'Cerqueira César', 'São Paulo', 'SP', '(11) 9893-22771', 'liliane.rosa@lapastina.com', '1980-07-07', 'F', 'EAT', '203040', 1, '189.51.11.226', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36', NULL, '2016-10-10 19:14:00', '2016-10-10 19:14:00'),
(5, 'LA PASTINA', 'LILIANE ROSA', '278.819.568-01', '01426-020', 'Rua Cristóvão Diniz', '67', '', 'Cerqueira César', 'São Paulo', 'SP', '(11) 9893-22771', 'liliane.rosa@lapastina.com', '1980-07-07', 'F', 'EAT', '203031', 1, '189.51.11.226', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36', NULL, '2016-10-10 19:31:17', '2016-10-10 19:31:17'),
(6, 'LA PASTINA', 'Aline Pires', '380.262.838-39', '09195-080', 'Rua Baependi', '101', '', 'Vila Alzira', 'Santo André', 'SP', '(11) 9682-86535', 'lih.pires@hotmail.com', '1990-06-10', 'F', 'eat', '202020', 1, '189.51.11.226', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36', NULL, '2016-10-10 19:36:58', '2016-10-10 19:36:58');

-- --------------------------------------------------------

--
-- Estrutura da tabela `receitas`
--

CREATE TABLE IF NOT EXISTS `receitas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ingredientes` text COLLATE utf8_unicode_ci NOT NULL,
  `preparo` text COLLATE utf8_unicode_ci NOT NULL,
  `tempo_preparo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rendimento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `receitas`
--

INSERT INTO `receitas` (`id`, `titulo`, `slug`, `thumb`, `ingredientes`, `preparo`, `tempo_preparo`, `rendimento`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 'Crosta de parmesão com Mix na Brasa', 'crosta-de-parmesao-com-mix-na-brasa', 'entrada-crosta-de-parmesao-com-mix-na-brasa-9.JPG', '<p>500g de parmes&atilde;o ralado grosso</p>\r\n\r\n<p>1 Abobrinha na brasa La Pastina</p>\r\n\r\n<p>1 Berinjela na brasa La Pastina</p>\r\n\r\n<p>1 Alcachofra na brasa La Pastina</p>\r\n\r\n<p>1 Piment&atilde;o na brasa La Pastina</p>\r\n\r\n<p>1 ma&ccedil;o de mini r&uacute;cula</p>\r\n\r\n<p>1 caixa de tomate-cereja</p>\r\n', '<p>Em uma frigideira antiaderente coloque uma camada fina de parmes&atilde;o at&eacute; cobrir todo o fundo, em fogo alto deixe at&eacute; que esteja dourado, tire e coloque em cima de uma x&iacute;cara para dar o formato de uma cesta. Deixe esfriar.</p>\r\n\r\n<p>Depois de frio disponha a r&uacute;cula e os legumes na brasa e sirva.</p>\r\n\r\n<p><strong>Harmonize com: </strong>Cono Sur Bicicleta Carm&eacute;ner&egrave;</p>\r\n', '20 minutos', '5 porções', 0, '2016-10-04 18:58:55', '2016-10-11 15:29:35'),
(2, 'Gnocchi de batatas ao Molho de Funghi Porcini', 'gnocchi-de-batatas-ao-molho-de-funghi-porcini', 'prato-principal-gnocchi-ao-molho-de-funghi-porcini-4.JPG', '<p>500g de Gnocchi La Pastina</p>\r\n\r\n<p>1 lata de 400g de Pomodori Cubetti La Pastina</p>\r\n\r\n<p>20g de Funghi Porcini La Pastina</p>\r\n\r\n<p>200ml de creme de leite fresco</p>\r\n\r\n<p>1 ma&ccedil;o de salsinha picada</p>\r\n\r\n<p>1 cebola picada em cubos pequenos</p>\r\n\r\n<p>4 dentes de alho amassados</p>\r\n\r\n<p>1 Azeite Org&acirc;nico Extravirgem La Pastina</p>\r\n\r\n<p>1 x&iacute;cara de &aacute;gua quente</p>\r\n\r\n<p>Sal Rosa do Himalaya Montosco a gosto</p>\r\n\r\n<p>Pimenta Preta em gr&atilde;os Montosco a gosto</p>\r\n\r\n<p>4 fil&eacute;s de peito de frango</p>\r\n', '<p>Hidrate o funghi em &aacute;gua quente, por 10 minutos. Em seguida corte em cubos e reserve a &aacute;gua.</p>\r\n\r\n<p>Em uma panela coloque um fio de azeite, frite o alho em seguida coloque a cebola, frite por alguns instantes, acrescente o funghi, apure por mais 2 minutos.</p>\r\n\r\n<p>Acrescente o pomodori cubetti, o creme de leite e 1 x&iacute;cara de &aacute;gua que voc&ecirc; hidratou o funghi, deixe reduzir por 10 minutos em fogo baixo corrija o sal e a pimenta.</p>\r\n\r\n<p>Cozinhe o gnocchi e grelhe os fil&eacute;s de frango temperados com sal e pimenta.</p>\r\n\r\n<p>Misture o molho ao gnocchi, acrescente a salsinha e sirva com os fil&eacute;s de frango grelhados.</p>\r\n\r\n<p><strong>Harmonize com:</strong> &nbsp;Torrevento Bolonero IGT</p>\r\n', '30 minutos', '4 porções', 0, '2016-10-04 19:00:23', '2016-10-11 15:30:05'),
(3, 'Arroz Negro com Camarões e Crips de Alho Poró ao Perfume de Limão Siciliano', 'arroz-negro-com-camaroes-e-crips-de-alho-poro-ao-perfume-de-limao-siciliano', 'setembro-arroz-negro-com-camaroes-e-crisps-de-alho-poro-ao-perfume-de-limao-siciliano.jpg', '<p>2 x&iacute;caras de Arroz Negro LA PASTINA</p>\r\n\r\n<p>&frac12; kg de Camar&atilde;o</p>\r\n\r\n<p>1 Alho Por&oacute; Fatiado</p>\r\n\r\n<p>1 Lim&atilde;o Siciliano (Tirar as Raspas da Casca para Decorar)</p>\r\n\r\n<p>Azeite</p>\r\n\r\n<p>Caldo de Legumes</p>\r\n\r\n<p>Sal</p>\r\n\r\n<p>Farinha de Trigo</p>\r\n\r\n<p>Pimenta do Reino</p>\r\n\r\n<p>Tomate Cereja Para Decorar</p>\r\n\r\n<p>&Oacute;leo</p>\r\n\r\n<p><strong>Receita B&aacute;sica do Caldo de Legumes:</strong></p>\r\n\r\n<p>1 Litro de &Aacute;gua</p>\r\n\r\n<p>1 Cenoura Cortada Grosseiramente</p>\r\n\r\n<p>1 Cebola Cortada em 4 Partes</p>\r\n\r\n<p>2 Talos de Sals&atilde;o</p>\r\n\r\n<p>1 Alho Por&oacute;</p>\r\n', '<p>Cozinhar o arroz na panela de press&atilde;o coberto com caldo de legumes por 5 minutos (depois que come&ccedil;ar a press&atilde;o)<br />\r\nTemperar com azeite e sal. Reserve.<br />\r\nEm uma panela pequena aquecer o &oacute;leo, enquanto isso, salpique farinha de trigo no alho poro, tirando o excesso.<br />\r\nFritar at&eacute; as fatias ficarem douradas. Escorrer no papel toalha e reservar.<br />\r\nNuma frigideira com azeite, fritar os camar&otilde;es temperando com gotas de lim&atilde;o siciliano, sal e pimenta do reino.<br />\r\nColocar o arroz no centro do prato e por cima os camar&otilde;es. Salpicar as raspinhas de lim&atilde;o.<br />\r\nDispor o alho poro em volta com os tomates cereja cortados ao meio.<br />\r\n&nbsp;</p>\r\n\r\n<p><strong>Harmonize com: </strong>Emiliana Adobe Ros&eacute;</p>\r\n', '30 minutos', '4 porções', 0, '2016-10-04 19:03:27', '2016-10-11 15:28:41'),
(4, 'Risoto de Alcachofra (Bruschetta e Coração) com Raspas de Limão Siciliano', 'risoto-de-alcachofra-bruschetta-e-coracao-com-raspas-de-limao-siciliano', 'riso.jpg', '<p>01 Pacote de Arroz Arb&oacute;reo ou Carnaroli LA PASTINA</p>\r\n\r\n<p>02 Litros de Caldo de Legumes</p>\r\n\r\n<p>01 Cebola Picada</p>\r\n\r\n<p>02 Colheres (Sopa) de Manteiga</p>\r\n\r\n<p>&frac12; Copo de Vinho Branco Seco</p>\r\n\r\n<p>01 Vidro de Bruschetta de Alcachofra LA PASTINA</p>\r\n\r\n<p>02 Vidros de Cora&ccedil;&atilde;o de Alcachofras LA PASTINA Cortados Ao Meio</p>\r\n\r\n<p>01 Pe&ccedil;a de Queijo Brie M&eacute;dio Picado</p>\r\n\r\n<p>01 X&iacute;cara de Parmes&atilde;o Ralado</p>\r\n\r\n<p>Raspas De Lim&atilde;o Siciliano</p>\r\n\r\n<p>Sal</p>\r\n\r\n<p>Pimenta Negra CARMENCITA</p>\r\n\r\n<p>Folhas De R&uacute;cula Higienizadas</p>\r\n', '<p>Numa panela aquecer o caldo e manter aquecido durante o processo do Risoto. Em outra panela, colocar uma colher de manteiga e deixar derreter, entrar com a cebola e deixar refogar at&eacute; ficar transparente. Adicionar o arroz e deixar fritar um pouco. Em seguida colocar o vinho e deixar o &aacute;lcool evaporar. Come&ccedil;ar a entrar com o caldo, colocando duas conchas por vez e mexendo sempre at&eacute; o caldo ir secando, continuar colocando o caldo aos poucos e em seguida acrescentar a bruschetta de alcachofra mexendo bem. Quando o arroz estiver cozido, por&eacute;m firme (al dente), colocar o queijo brie e os cora&ccedil;&otilde;es de alcachofras. Quando o brie derreter, coloque o parmes&atilde;o, corrija o sal e acrescente a pimenta negra. Por &uacute;ltimo, coloque a outra colher de manteiga para dar cremosidade e brilho. Servir decorando com as folhas de r&uacute;cula. Finalizar com raspas de lim&atilde;o siciliano.</p>\r\n\r\n<p><strong>Harmonize com: </strong>Cono Sur Reserva Especial Sauvignon Blanc</p>\r\n', '30 minutos', '6 porções', 0, '2016-10-04 19:05:03', '2016-10-11 15:31:46'),
(5, 'Picolé Quinoa Mix Doce La Pastina', 'picole-quinoa-mix-doce-la-pastina', 'quinoa-06.JPG', '<p>110g de Quinoa Mix Doce La Pastina (sabor de sua prefer&ecirc;ncia)</p>\r\n\r\n<p>1 bola de sorvete creme</p>\r\n\r\n<p>Use a colher da embalagem como palito</p>\r\n', '<p>Coloque a Quinoa Mix Doce aos poucos dentro da forma de picol&eacute; em seguida coloque a colher no centro da forma e leve ao congelador por 3 horas.</p>\r\n\r\n<p><strong>Harmonize com: </strong>Emiliana Late Harvest</p>\r\n', '5 minutos', '2 porções', 0, '2016-10-04 19:06:02', '2016-10-11 15:31:12'),
(6, 'Couscous marroquino envolto de vegetais e pesto ', 'couscous-marroquino-envolto-de-vegetais-e-pesto', 'cs.jpg', '<p>250 g de couscous marroquino La Pastina</p>\r\n\r\n<p>1 tablete dilu&iacute;do em &aacute;gua quente de caldo de frango Feriolli</p>\r\n\r\n<p>3 vidros de Mix de Vegetais na brasa La Pastina</p>\r\n\r\n<p>1 vidro de Pesto Genovese La Pastina</p>\r\n\r\n<p>Sal e pimenta preta Montosco a gosto</p>\r\n', '<p>Hidrate o couscous com o fundo de legumes.</p>\r\n\r\n<p>Em seguida pegue um vidro de vegetais e pique em cubos, misture no couscous, junto com o &oacute;leo que vem na conserva. Ajuste o sal e a pimenta e reserve.</p>\r\n\r\n<p>Em um furador redondo de 5 ou 6 cm, pegue os vegetais que sobraram e coloque nas paredes do furador, depois coloque o couscous no centro e aperte, desenforme e sirva com o Pesto.</p>\r\n\r\n<p><strong>Harmonize com:</strong> Bicicleta Sauvignon Blanc</p>\r\n', '30 Minutos ', '5 porções', 0, '2016-10-04 20:04:02', '2016-10-11 15:29:11'),
(7, 'Gnocchi grelhado com sal grosso e alecrim ao ketchup balsamic La Pastina', 'gnocchi-grelhado-com-sal-grosso-e-alecrim-ao-ketchup-balsamic-la-pastina', 'entrada-gnocchi-grelhado-imagem.JPG', '<p>500 g de Gnocchi La Pastina</p>\r\n\r\n<p>50g de manteiga</p>\r\n\r\n<p>10 ml de Azeite de Oliva Extravirgem La Pastina</p>\r\n\r\n<p>5 g de sal grosso</p>\r\n\r\n<p>Alecrim e Sal Montosco a gosto</p>\r\n', '<p>Em uma frigideira antiaderente, coloque a manteiga e o azeite, grelhe o gnocchi at&eacute; que fique dourado, em seguida coloque o sal grosso e o alecrim.<br />\r\nSirva com ketchup balsamic La Pastina.</p>\r\n\r\n<p><strong>Harmonize com:</strong> Sangiovese Toscana Cecchi</p>\r\n', '20 minutos', '5 porções', 0, '2016-10-04 20:05:35', '2016-10-11 15:25:47'),
(8, 'Salada de arroz vermelho ao mediterrâneo', 'salada-de-arroz-vermelho-ao-mediterraneo', 'salada-de-arroz-vermelho-ao-mediterraneo.jpg', '<p>500 g Arroz Vermelho La Pastina</p>\r\n\r\n<p>3 latas de Atum Claro S&oacute;lido em azeite Albo</p>\r\n\r\n<p>1 vidro de Azeitonas Pretas fatiadas La Pastina 160 g</p>\r\n\r\n<p>1 vidro de Alho Aperitivo em Azeite La Pastina de 160 g cortado em l&acirc;minas</p>\r\n\r\n<p>1 vidro de Pepininho em Azeite de Oliva La Pastina 160 g cortado em l&acirc;minas</p>\r\n\r\n<p>1 lata de P&ecirc;ssego em Calda La Pastina cortado em l&acirc;minas</p>\r\n\r\n<p>Sal, Pimenta Negra em Gr&atilde;os e Salsinha a gosto</p>\r\n\r\n<p>Azeite extravirgem La Pastina</p>\r\n\r\n<p>Aceto bals&acirc;mico La Pastina</p>\r\n', '<p>Cozinhe o arroz conforme as especifica&ccedil;&otilde;es da embalagem, deixe esfriar. Quando o arroz estiver frio, misture as azeitonas, alho, pepininho e os p&ecirc;ssegos. Regue com azeite, salpique a salsinha, coloque a gosto a aceto bals&acirc;mico. Por &uacute;ltimo coloque a pimenta e corrija o sal. Sirva em uma travessa preferencialmente branca para que sua salada de um destaque.</p>\r\n\r\n<p><strong>Harmonize com:</strong> Emiliana Novas Chardonay</p>\r\n', '30 minutos', '10 porções', 0, '2016-10-04 20:07:07', '2016-10-11 15:27:19'),
(9, 'Risoto de Filé de Frango ao Molho de Mostarda', 'risoto-de-file-de-frango-ao-molho-de-mostarda', 'riss.jpg', '<p>500 g de fil&eacute; frango em tiras finas (pode substituir por carne)&nbsp;</p>\r\n\r\n<p>450 g de Arroz para Risoto La Pastina</p>\r\n\r\n<p>1 cebola picada</p>\r\n\r\n<p>4 colheres de Azeite Extravirgem La Pastina</p>\r\n\r\n<p>200ml de vinho branco seco</p>\r\n\r\n<p>2 litros de caldo de carne Feriolli</p>\r\n\r\n<p>Queijo parmes&atilde;o ralado</p>\r\n\r\n<p>03 colheres de sopa de mostarda Dijon Beaufor</p>\r\n\r\n<p>Sal e Pimenta Negra Carmencita a gosto</p>\r\n\r\n<p>&frac12; lata de creme de leite fresco</p>\r\n\r\n<p>Manteiga para finalizar</p>\r\n', '<p>Aque&ccedil;a o caldo em uma panela separada e deixe-o ferver. Doure a cebola no azeite e acrescente o arroz. Frite um pouco e junte o vinho branco at&eacute; que esse evapore. Acrescente o caldo aos poucos, uma concha de cada vez, sem que essa cubra todo o arroz, e mexa at&eacute; evaporar por completo. Fa&ccedil;a isso at&eacute; que o caldo acabe ou at&eacute; o arroz ficar al dente. Quando estiver quase no ponto, aque&ccedil;a uma frigideira com azeite frite as tiras de fil&eacute;, temperadas com sal e pimenta negra. Acrescente as tiras ao arroz, a mostarda e o creme de leite. Mexa e adicione a manteiga e o queijo ralado, mexendo bem j&aacute; com o fogo apagado. Adicione o sal se necess&aacute;rio.</p>\r\n\r\n<p><strong>Harmonize com:</strong> Chakana Malbec Ros&eacute;</p>\r\n', '30 minutos', '5 porções', 0, '2016-10-04 20:08:57', '2016-10-11 15:26:27'),
(10, 'Spaghetti com molho Pesto Genovese La Pastina', 'spaghetti-com-molho-pesto-genovese-la-pastina', 'massa-ao-pesto-01.JPG', '<p>200g de Spaghetti Trafilata al Bronzo Divella</p>\r\n\r\n<p>90g de molho Pesto Genovese La Pastina</p>\r\n', '<p>Cozinhe a massa conforme as especifica&ccedil;&otilde;es da embalagem.</p>\r\n\r\n<p>Quando a massa estiver al dente escorra a &aacute;gua deixando uma pequena quantidade. Adicione o molho e mexa por 2 minutos.</p>\r\n\r\n<p><strong>Harmonize com:</strong> Cono Sur Bicicleta Chardonay</p>\r\n', '2 porções', '15 minutos', 0, '2016-10-04 20:10:01', '2016-10-11 15:32:17'),
(11, 'Mousse de Pêssego com Calda de Frutas do Bosque', 'mousse-de-pessego-com-calda-de-frutas-do-bosque', 'sobremesa-mousse-de-pessego-com-geleia-de-frutas-do-bosque-6.JPG', '<p>1 lata de P&ecirc;ssego Diet La Pastina</p>\r\n\r\n<p>1 lata de leite condensado Diet</p>\r\n\r\n<p>1 lata de creme de leite Diet</p>\r\n\r\n<p>1 gelatina de p&ecirc;ssego Diet</p>\r\n\r\n<p>1 geleia de Frutas do bosque Casa Giulia</p>\r\n\r\n<p>100ml de vinho branco</p>\r\n', '<p>Coloque no liquidificador os p&ecirc;ssegos, o leite condensado e o creme de leite, bata por 5 minutos. Dissolva a gelatina em 1 copo de &aacute;gua quente e acrescente no liquidificador, bata por mais 2 minutos. Coloque em uma lou&ccedil;a de sua prefer&ecirc;ncia,</p>\r\n\r\n<p><strong>Harmonize com:</strong> Riesling Cono Sur Cosecha Noble</p>\r\n', '30 minutos', '12 porções', 0, '2016-10-04 20:11:29', '2016-10-11 15:30:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_login_unique` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `nome`, `login`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Trupe', 'trupe', 'contato@trupe.net', '$2y$10$S1ivVS/dKHETao906az3cePFZydz30/yRmsocuewuxRT4bjWOze56', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
